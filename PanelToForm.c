/******************************************************************************
  PROGRAM: PanelToForm
  
  PURPOSE: Takes a UIR file and generate an HTML form with similar controls
  			and a C source file meant to be compiled into a cgi-script for the reception
  			of the form, thus allowing you to transform a classic CVI application to a 
  			Web application.
  
  NEEDS: you need to link the resulting C code with cgic.c v1.06
******************************************************************************/

#include <formatio.h>
#include <utility.h>
#include <ansi_c.h>
#include <cvirte.h>		
#include <userint.h>

#include "iso646.h"
#include "PanelToForm.h"

static int Pnl;
static int HtmlFile=-2, CFile=-2;

int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((Pnl = LoadPanel (0, "PanelToForm.uir", PANEL)) < 0)
		return -1;
	DisplayPanel (Pnl);

	RunUserInterface ();

	DiscardPanel (Pnl);
	return 0;
}


/******************************************************************************
  FUNCTION: Replace
  PURPOSE: Change a specific text in a string.
  IN: Txt: String to seearch
  	  From: substring to change from
  	  To: substring to change to
  NOTE: calls can be re-entrant, ex:
  		Replace(Replace(Replace(Replace(Msg, "&", "&amp;"),">", "&gt;"),"<", "&lt;"),"\n", "<BR>\n")
******************************************************************************/
char* Replace(const char* Txt, const char* From, const char* To) {
	static char Str[2000]="";
	char* Pos;
	strcpy(Str, Txt);
	while ((Pos=strstr(Str, From))!=NULL) {
		memmove(Pos+strlen(To), Pos+strlen(From), Pos-Str+strlen(Str)-strlen(From)+1);
		memcpy(Pos, To, strlen(To));		
	}
	return Str;
}


static char* StrMode(const int Mode) {
	switch (Mode) {
		case VAL_INDICATOR:return "indicator";
		case VAL_HOT:return "hot";
		case VAL_NORMAL:return "normal";
		case VAL_VALIDATE:return "notify";
	}
	return "";	
}

																
#define ShowH SetCtrlVal(Pnl, PANEL_HTML_CODE, StrHtml); WriteFile(HtmlFile, StrHtml, strlen(StrHtml))
#define ShowC SetCtrlVal(Pnl, PANEL_C_CODE, StrC); WriteFile(CFile, StrC, strlen(StrC))
static char StrHtml[1000], StrC[1000];

static void ExplorePanel(const int PnlExplore, const int PnlConstNum, const char* FileName) {
	int Ctrl, Style, Mode, Visible, Dimmed, DataType, StrLen, NbItems, i;
	double D, Dmin, Dmax;
	float F, Fmin, Fmax;
	char C, Cmin, Cmax;
	unsigned char UC, UCmin, UCmax;
	short S, Smin, Smax;
	unsigned short US, USmin, USmax;
	long L, Lmin, Lmax;
	unsigned long U, Umin, Umax;
	int I;
	BOOL Active, NoCallback;
	char Str[1000], StrButton[2000]="", Strmin[1000], Strmax[1000], 
		Title[100], OnText[100], OffText[100], 
		CallbackName[100], 
		ConstantName[100], ConstNamePnlCtrl[100], 
		PnlConstName[100], PnlHandleName[100], *Pos;
	
	GetPanelAttribute (PnlExplore, ATTR_TITLE, Str);
	GetPanelAttribute (PnlExplore, ATTR_CONSTANT_NAME, PnlConstName);
	PnlHandleName[0]='h';
	strcpy(PnlHandleName+1, PnlConstName);
	StringLowerCase (PnlHandleName);

	sprintf(StrHtml, "\n\n<BR>\n<FIELDSET>\n<LEGEND>Panel %d \"%s\"</LEGEND>\n", PnlConstNum, Str); ShowH;
	sprintf(StrC, "\n\nvoid InterpretPanel%d(void) {\n\tdouble D;\n\tint i,I,Invalid;\n\tchar *Str, DefaultStr[255], CtrlConst[100];"
					"\n\tchar *Switch[2]={\"0\", \"1\"};"
					"\n\tint %s=LoadPanel(0, \"%s\", %s);", PnlConstNum, PnlHandleName, FileName, PnlConstName); ShowC;

	GetPanelAttribute (PnlExplore, ATTR_PANEL_FIRST_CTRL, &Ctrl);
	while (Ctrl>0) {
		Title[0]='\0';
		GetCtrlAttribute(PnlExplore, Ctrl, ATTR_CTRL_STYLE, &Style);
		GetCtrlAttribute(PnlExplore, Ctrl, ATTR_CTRL_MODE, &Mode);
		GetCtrlAttribute(PnlExplore, Ctrl, ATTR_VISIBLE, &Visible);
		GetCtrlAttribute(PnlExplore, Ctrl, ATTR_DIMMED, &Dimmed);
		Active = Mode!=VAL_INDICATOR and Visible and !Dimmed;
		GetCtrlAttribute (PnlExplore, Ctrl, ATTR_CONSTANT_NAME, ConstantName);
		sprintf(ConstNamePnlCtrl, "%s_%s", PnlConstName, ConstantName);
		GetCtrlAttribute (PnlExplore, Ctrl, ATTR_CALLBACK_NAME, CallbackName);
		if (NoCallback=(CallbackName[0]=='\0')) strcpy(CallbackName, "// NoCallback");
		switch (Style) {
			case CTRL_NUMERIC:
			case CTRL_NUMERIC_THERMOMETER:
			case CTRL_NUMERIC_TANK:
			case CTRL_NUMERIC_GAUGE:
			case CTRL_NUMERIC_METER:
			case CTRL_NUMERIC_KNOB:
			case CTRL_NUMERIC_DIAL:
			case CTRL_NUMERIC_VSLIDE:
			case CTRL_NUMERIC_HSLIDE:
			case CTRL_NUMERIC_FLAT_VSLIDE:
			case CTRL_NUMERIC_FLAT_HSLIDE:
			case CTRL_NUMERIC_LEVEL_VSLIDE:
			case CTRL_NUMERIC_LEVEL_HSLIDE:
			case CTRL_NUMERIC_POINTER_VSLIDE:
			case CTRL_NUMERIC_POINTER_HSLIDE:
			case CTRL_COLOR_NUMERIC:
				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_LABEL_TEXT, Title);	 
				strcpy(Title, Replace(Title, "__", ""));
				if (Title[0]=='\0') sprintf(Title, "Empty numeric label (%s)", ConstNamePnlCtrl);
				sprintf(Str, "%d Numeric %s \"%s\", %s %s %s", Ctrl, ConstNamePnlCtrl, Title, StrMode(Mode), Visible?"":"invisible", Dimmed?"dimmed":"");
				sprintf(StrHtml, "\n\n<!-- %s -->", Str); ShowH;
				sprintf(StrC, "\n\n\t// %s", Str);			ShowC;

				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_DATA_TYPE, &DataType);
				switch (DataType) {
				    case VAL_CHAR:
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_DFLT_VALUE, &C);
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_MAX_VALUE, &Cmax);
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_MIN_VALUE, &Cmin);
						sprintf(StrHtml, "\n<%sINPUT TYPE=text NAME=%s VALUE=\"%d\" MAXLENGTH=4> %s<BR%s>", 
								Active?"":"!--", ConstNamePnlCtrl, C, Title, Active?"":"--"); ShowH;
						sprintf(StrC, "\n\t%scgiFormIntegerBounded(\"%s\", &I, %d, %d, %d);"
										"\n\t%sSetCtrlVal(%s, %s, I);"
										"\n\t%s%s(%s, %s, EVENT_COMMIT, NULL, 0, 0);"
										"\n\t%sfprintf(cgiOut, \"\\n<P>%s set to %%d.</P>\", I);", 
								Active?"":"// ", ConstNamePnlCtrl, Cmin, Cmax, C,
								Active?"":"// ", PnlHandleName, ConstNamePnlCtrl, 
								Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
								Active?"":"// ", Title); ShowC;
						break;
				    case VAL_INTEGER:
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_DFLT_VALUE, &L);
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_MAX_VALUE, &Lmax);
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_MIN_VALUE, &Lmin);
						sprintf(StrHtml, "\n<%sINPUT TYPE=text NAME=%s VALUE=\"%d\" MAXLENGTH=11> %s<BR%s>", 
								Active?"":"!--", ConstNamePnlCtrl, L, Title, Active?"":"--"); ShowH;
						sprintf(StrC, "\n\t%scgiFormIntegerBounded(\"%s\", &I, %d, %d, %d);"
										"\n\t%sSetCtrlVal(%s, %s, I);"
										"\n\t%s%s(%s, %s, EVENT_COMMIT, NULL, 0, 0);"
										"\n\t%sfprintf(cgiOut, \"\\n<P>%s set to %%d.</P>\", I);", 
								Active?"":"// ", ConstNamePnlCtrl, Lmin, Lmax, L,
								Active?"":"// ", PnlHandleName, ConstNamePnlCtrl, 
								Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
								Active?"":"// ", Title); ShowC;
						break;
				    case VAL_SHORT_INTEGER:
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_DFLT_VALUE, &S);
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_MAX_VALUE, &Smax);
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_MIN_VALUE, &Smin);
						sprintf(StrHtml, "\n<%sINPUT TYPE=text NAME=%s VALUE=\"%d\" MAXLENGTH=6> %s<BR%s>", 
								Active?"":"!--", ConstNamePnlCtrl, S, Title, Active?"":"--"); ShowH;
						sprintf(StrC, "\n\t%scgiFormIntegerBounded(\"%s\", &I, %d, %d, %d);"
										"\n\t%sSetCtrlVal(%s, %s, I);"
										"\n\t%s%s(%s, %s, EVENT_COMMIT, NULL, 0, 0);" 
										"\n\t%sfprintf(cgiOut, \"\\n<P>%s set to %%d.</P>\", I);", 
								Active?"":"// ", ConstNamePnlCtrl, Smin, Smax, S,
								Active?"":"// ", PnlHandleName, ConstNamePnlCtrl, 
								Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
								Active?"":"// ", Title); ShowC;
						break;
				    case VAL_UNSIGNED_CHAR:
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_DFLT_VALUE, &UC);
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_MAX_VALUE, &UCmax);
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_MIN_VALUE, &UCmin);
						sprintf(StrHtml, "\n<%sINPUT TYPE=text NAME=%s VALUE=\"%d\" MAXLENGTH=3> %s<BR%s>", 
								Active?"":"!--", ConstNamePnlCtrl, UC, Title, Active?"":"--"); ShowH;
						sprintf(StrC, "\n\t%scgiFormIntegerBounded(\"%s\", &I, %d, %d, %d);"
										"\n\t%sSetCtrlVal(%s, %s, I);"
										"\n\t%s%s(%s, %s, EVENT_COMMIT, NULL, 0, 0);" 
										"\n\t%sfprintf(cgiOut, \"\\n<P>%s set to %%d.</P>\", I);", 
								Active?"":"// ", ConstNamePnlCtrl, UCmin, UCmax, UC,
								Active?"":"// ", PnlHandleName, ConstNamePnlCtrl, 
								Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
								Active?"":"// ", Title); ShowC;
						break;
				    case VAL_UNSIGNED_SHORT_INTEGER:
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_DFLT_VALUE, &US);
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_MAX_VALUE, &USmax);
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_MIN_VALUE, &USmin);
						sprintf(StrHtml, "\n<%sINPUT TYPE=text NAME=%s VALUE=\"%d\" MAXLENGTH=5> %s<BR%s>", 
								Active?"":"!--", ConstNamePnlCtrl, US, Title, Active?"":"--"); ShowH;
						sprintf(StrC, "\n\t%scgiFormIntegerBounded(\"%s\", &I, %d, %d, %d);"
										"\n\t%sSetCtrlVal(%s, %s, I);"
										"\n\t%s%s(%s, %s, EVENT_COMMIT, NULL, 0, 0);" 
										"\n\t%sfprintf(cgiOut, \"\\n<P>%s set to %%d.</P>\", I);", 
								Active?"":"// ", ConstNamePnlCtrl, USmin, USmax, US,
								Active?"":"// ", PnlHandleName, ConstNamePnlCtrl, 
								Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
								Active?"":"// ", Title); ShowC;
						break;
				    case VAL_UNSIGNED_INTEGER:
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_DFLT_VALUE, &U);
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_MAX_VALUE, &Umax);
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_MIN_VALUE, &Umin);
						sprintf(StrHtml, "\n<%sINPUT TYPE=text NAME=%s VALUE=\"%d\" MAXLENGTH=10> %s<BR%s>", 
								Active?"":"!--", ConstNamePnlCtrl, U, Title, Active?"":"--"); ShowH;
						sprintf(StrC, "\n\t%scgiFormIntegerBounded(\"%s\", &I, %d, %d, %d);"
										"\n\t%sSetCtrlVal(%s, %s, I);"
										"\n\t%s%s(%s, %s, EVENT_COMMIT, NULL, 0, 0);"
										"\n\t%sfprintf(cgiOut, \"\\n<P>%s set to %%d.</P>\", I);", 
								Active?"":"// ", ConstNamePnlCtrl, Umin, Umax, U,
								Active?"":"// ", PnlHandleName, ConstNamePnlCtrl, 
								Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
								Active?"":"// ", Title); ShowC;
						break;
				    case VAL_FLOAT:
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_DFLT_VALUE, &F);
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_MAX_VALUE, &Fmax);
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_MIN_VALUE, &Fmin);
						sprintf(StrHtml, "\n<%sINPUT TYPE=text NAME=%s VALUE=\"%g\"> %s<BR%s>", 
								Active?"":"!--", ConstNamePnlCtrl, F, Title, Active?"":"--"); ShowH;
						if (Fmin<-1e30 and Fmax>1e30) {
							sprintf(StrC, "\n\t%scgiFormDouble(\"%s\", &D, %g);"
											"\n\t%sSetCtrlVal(%s, %s, D);"
											"\n\t%s%s(%s, %s, EVENT_COMMIT, NULL, 0, 0);" 
											"\n\t%sfprintf(cgiOut, \"\\n<P>%s set to %%g.</P>\", D);", 
									Active?"":"// ", ConstNamePnlCtrl, F,
									Active?"":"// ", PnlHandleName, ConstNamePnlCtrl, 
									Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
									Active?"":"// ", Title); ShowC;
						} else {
							sprintf(StrC, "\n\t%scgiFormDoubleBounded(\"%s\", &D, %g, %g, %g);"
											"\n\t%sSetCtrlVal(%s, %s, D);"
											"\n\t%s%s(%s, %s, EVENT_COMMIT, NULL, 0, 0);" 
											"\n\t%sfprintf(cgiOut, \"\\n<P>%s set to %%g.</P>\", D);", 
									Active?"":"// ", ConstNamePnlCtrl, Fmin, Fmax, F,
									Active?"":"// ", PnlHandleName, ConstNamePnlCtrl, 
									Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
									Active?"":"// ", Title); ShowC;
						}
						break;
				    case VAL_DOUBLE:
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_DFLT_VALUE, &D);
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_MAX_VALUE, &Dmax);
						GetCtrlAttribute (PnlExplore, Ctrl, ATTR_MIN_VALUE, &Dmin);
						sprintf(StrHtml, "\n<%sINPUT TYPE=text NAME=%s VALUE=\"%g\"> %s<BR%s>", 
								Active?"":"!--", ConstNamePnlCtrl, D, Title, Active?"":"--"); ShowH;
						if (Dmin<-1e30 and Dmax>1e30) {
							sprintf(StrC, "\n\t%scgiFormDouble(\"%s\", &D, %g);"
											"\n\t%sSetCtrlVal(%s, %s, D);"
											"\n\t%s%s(%s, %s, EVENT_COMMIT, NULL, 0, 0);"
											"\n\t%sfprintf(cgiOut, \"\\n<P>%s set to %%g.</P>\", D);", 
									Active?"":"// ", ConstNamePnlCtrl, D,
									Active?"":"// ", PnlHandleName, ConstNamePnlCtrl, 
									Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
									Active?"":"// ", Title); ShowC;
						} else {
							sprintf(StrC, "\n\t%scgiFormDoubleBounded(\"%s\", &D, %g, %g, %g);"
											"\n\t%sSetCtrlVal(%s, %s, D);"
											"\n\t%s%s(%s, %s, EVENT_COMMIT, NULL, 0, 0);" 
											"\n\t%sfprintf(cgiOut, \"\\n<P>%s set to %%g.</P>\", D);", 
									Active?"":"// ", ConstNamePnlCtrl, Dmin, Dmax, D,
									Active?"":"// ", PnlHandleName, ConstNamePnlCtrl, 
									Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
									Active?"":"// ", Title); ShowC;
						}
						break;
				    //case VAL_STRING:
				}
				break;				

			

			///////////////////////////////////////////////////////////////////
			case CTRL_STRING:				
				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_LABEL_TEXT, Title);	 
				strcpy(Title, Replace(Title, "__", ""));
				if (Title[0]=='\0') sprintf(Title, "Empty string label (%s)", ConstNamePnlCtrl);
				sprintf(Str, "%d String %s \"%s\", %s %s %s", Ctrl, ConstNamePnlCtrl, Title, StrMode(Mode), Visible?"":"invisible", Dimmed?"dimmed":"");
				sprintf(StrHtml, "\n\n<!-- %s -->", Str); ShowH;
				sprintf(StrC, "\n\n\t// %s", Str);			ShowC;

				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_DFLT_VALUE, Str);
				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_MAX_ENTRY_LENGTH, &StrLen);
				if (StrLen>0) sprintf(Strmax, " MAXLENGTH=%d", StrLen); else Strmax[0]='\0';
				sprintf(StrHtml, "\n<%sINPUT TYPE=text NAME=%s VALUE=\"%s\"%s> %s<BR%s>", 
						Active?"":"!--", ConstNamePnlCtrl, Str, Strmax, Title, Active?"":"--"); ShowH;
				if (StrLen>0) { 
					sprintf(StrC, "\n\t%schar Str%d[%d];\t// Move this line to the top of the function"
								"\n\t%scgiFormStringNoNewlines(\"%s\", Str%d, %d);"
								"\n\t%sSetCtrlVal(%s, %s, Str%d);"
								"\n\t%s%s(%s, %s, EVENT_COMMIT, NULL, 0, 0);"
								"\n\t%sfprintf(cgiOut, \"\\n<P>%s set to \\\"%%s\\\".</P>\", Str%d);", 
						Active?"":"// ", Ctrl, StrLen+1,
						Active?"":"// ", ConstNamePnlCtrl, Ctrl, StrLen,
						Active?"":"// ", PnlHandleName, ConstNamePnlCtrl, Ctrl, 
						Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
						Active?"":"// ", Title, Ctrl); ShowC;
				} else { 
					sprintf(StrC, "\n\t%scgiFormStringSpaceNeeded(\"%s\", &I);"
								"\n\t%sStr=(char*)malloc(I);"
								"\n\t%scgiFormStringNoNewlines(\"%s\", Str, I);"
								"\n\t%sSetCtrlVal(%s, %s, Str);"
								"\n\t%s%s(%s, %s, EVENT_COMMIT, NULL, 0, 0);"
								"\n\t%sfprintf(cgiOut, \"\\n<P>%s set to \\\"%%s\\\".</P>\", Str);"
								"\n\t%sfree(Str); Str=NULL;", 
						Active?"":"// ", ConstNamePnlCtrl,
						Active?"":"// ", 
						Active?"":"// ", ConstNamePnlCtrl, 
						Active?"":"// ", PnlHandleName, ConstNamePnlCtrl, 
						Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
						Active?"":"// ", Title,
						Active?"":"// "); ShowC;
				}
				break;

			
			///////////////////////////////////////////////////////////////////
			case CTRL_TEXT_MSG:
				GetCtrlVal(PnlExplore, Ctrl, Str);
				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_CONSTANT_NAME, ConstNamePnlCtrl);
				sprintf(StrHtml, "\n\n<!-- %d Message %s, %s %s %s -->", 
								Ctrl, ConstNamePnlCtrl, StrMode(Mode), Visible?"":"invisible", Dimmed?"dimmed":""); ShowH;
				sprintf(StrHtml, "\n<P>%s</P>", 
						Replace(Replace(Replace(Replace(Str, 
						"&", "&amp;"),">", "&gt;"),"<", "&lt;"),"\n", "<BR>")); ShowH;
				break;

			
			///////////////////////////////////////////////////////////////////
			case CTRL_TEXT_BOX:
				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_LABEL_TEXT, Title);	 
				strcpy(Title, Replace(Title, "__", ""));
				if (Title[0]=='\0') sprintf(Title, "Empty textbox label (%s)", ConstNamePnlCtrl);
				sprintf(Str, "%d Textbox %s \"%s\", %s %s %s", Ctrl, ConstNamePnlCtrl, Title, StrMode(Mode), Visible?"":"invisible", Dimmed?"dimmed":"");
				sprintf(StrHtml, "\n\n<!-- %s -->", Str); ShowH;
				sprintf(StrC, "\n\n\t// %s", Str);			ShowC;

				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_DFLT_VALUE, Str);
				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_MAX_ENTRY_LENGTH, &StrLen);	
				sprintf(StrHtml, "\n<%sTEXTAREA NAME=%s>%s</TEXTAREA> %s<BR%s>", 
						Active?"":"!--", ConstNamePnlCtrl, Str,
						Title, Active?"":"--"); ShowH;
				if (StrLen>0) { 
					sprintf(StrC, "\n\t%schar Str%d[%d];\t// Move this line to the top of the function"
								"\n\t%scgiFormString(\"%s\", Str%d, %d);"
								"\n\t%sResetTextBox(%s, %s, Str%d);"
								"\n\t%s%s(%s, %s, EVENT_COMMIT, NULL, 0, 0);"
								"\n\t%sfprintf(cgiOut, \"\\n<P>%s set to \\\"<PRE>%%s</PRE>\\\".</P>\", Str%d);",
						Active?"":"// ", Ctrl, StrLen+1,
						Active?"":"// ", ConstNamePnlCtrl, Ctrl, StrLen,
						Active?"":"// ", PnlHandleName, ConstNamePnlCtrl, Ctrl,
						Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
						Active?"":"// ", Title, Ctrl); ShowC;
				} else { 
					sprintf(StrC, "\n\t%scgiFormStringSpaceNeeded(\"%s\", &I);"
								"\n\t%sStr=(char*)malloc(I);"
								"\n\t%scgiFormString(\"%s\", Str, I);"
								"\n\t%sResetTextBox(%s, %s, Str);"
								"\n\t%s%s(%s, %s, EVENT_COMMIT, NULL, 0, 0);"
								"\n\t%sfprintf(cgiOut, \"\\n<P>%s set to \\\"<PRE>%%s</PRE>\\\".</P>\", Str);"
								"\n\t%sfree(Str); Str=NULL;", 
						Active?"":"// ", ConstNamePnlCtrl,
						Active?"":"// ", 
						Active?"":"// ", ConstNamePnlCtrl, 
						Active?"":"// ", PnlHandleName, ConstNamePnlCtrl, 
						Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
						Active?"":"// ", Title,
						Active?"":"// "); ShowC;
				}
				break;


			///////////////////////////////////////////////////////////////////
			case CTRL_SQUARE_COMMAND_BUTTON:
			case CTRL_OBLONG_COMMAND_BUTTON:
			case CTRL_ROUND_COMMAND_BUTTON:
			case CTRL_ROUNDED_COMMAND_BUTTON:
			case CTRL_PICTURE_COMMAND_BUTTON:
			case CTRL_ROUND_BUTTON:
			case CTRL_SQUARE_BUTTON:
			case CTRL_ROUND_FLAT_BUTTON:
			case CTRL_SQUARE_FLAT_BUTTON:
			case CTRL_ROUND_RADIO_BUTTON:
			case CTRL_SQUARE_RADIO_BUTTON:
				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_LABEL_TEXT, Title); 
				strcpy(Title, Replace(Title, "__", ""));
				if (Title[0]=='\0') sprintf(Title, "Empty button label (%s)", ConstNamePnlCtrl);
				sprintf(Str, "%d Button %s \"%s\", %s %s %s", 
						Ctrl, ConstNamePnlCtrl, Title, StrMode(Mode), Visible?"":"invisible", Dimmed?"dimmed":"");
				sprintf(StrHtml, "\n\n<!-- %s -->", Str); ShowH;
				sprintf(StrButton, "%s\n\n\t// %s", StrButton, Str);

				sprintf(StrHtml, "\n<%sINPUT TYPE=submit NAME=%s VALUE=\"%s\"><BR%s>", 
						Active?"":"!--", ConstNamePnlCtrl, Title, Active?"":"--"); ShowH;
				sprintf(StrButton, "%s\n\t%scgiFormStringNoNewlines(\"%s\", CtrlConst, 100);"
							"\n\t%sif (strlen(CtrlConst)>0) %s(%s, %s, EVENT_COMMIT, NULL, 0, 0);"
							"\n\t%sfprintf(cgiOut, \"\\n<P>Submit button pressed: [<B>%s</B>].</P>\");", 
					StrButton, 
					Active?"":"// ", ConstNamePnlCtrl, 
					Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
					Active?"":"// ", Title);
				break;

			
			///////////////////////////////////////////////////////////////////
			case CTRL_CHECK_BOX:
			case CTRL_ROUND_PUSH_BUTTON:
			case CTRL_SQUARE_PUSH_BUTTON:
			case CTRL_ROUND_PUSH_BUTTON2:
			case CTRL_SQUARE_PUSH_BUTTON2:
			case CTRL_SQUARE_TEXT_BUTTON:
			case CTRL_OBLONG_TEXT_BUTTON:
			case CTRL_ROUND_TEXT_BUTTON:
			case CTRL_ROUNDED_TEXT_BUTTON:
			case CTRL_PICTURE_TOGGLE_BUTTON:

			case CTRL_ROUND_LIGHT:			// even though those should only be indicators...
			case CTRL_SQUARE_LIGHT:
			case CTRL_ROUND_LED:
			case CTRL_SQUARE_LED:
				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_LABEL_TEXT, Title);	 
				strcpy(Title, Replace(Title, "__", ""));
				if (Title[0]=='\0') sprintf(Title, "Empty checkbox label (%s)", ConstNamePnlCtrl);
				sprintf(Str, "%d Checkbox %s \"%s\", %s %s %s", Ctrl, ConstNamePnlCtrl, Title, StrMode(Mode), Visible?"":"invisible", Dimmed?"dimmed":"");
				sprintf(StrHtml, "\n\n<!-- %s -->", Str); ShowH;
				sprintf(StrC, "\n\n\t// %s", Str);			ShowC;

				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_DFLT_VALUE, &I);
				sprintf(StrHtml, "\n<%sINPUT TYPE=checkbox NAME=%s%s> %s<BR%s>", 
						Active?"":"!--", ConstNamePnlCtrl, I?" CHECKED":"", Title, Active?"":"--"); ShowH;
				sprintf(StrC, "\n\t%sI=(cgiFormCheckboxSingle(\"%s\") == cgiFormSuccess);"
								"\n\t%sSetCtrlVal(%s, %s, I);"
								"\n\t%s%s(%s, %s, EVENT_COMMIT, NULL, 0, 0);"
								"\n\t%sfprintf(cgiOut, \"\\n<P>%s set to %%d.</P>\", I);", 
						Active?"":"// ", ConstNamePnlCtrl,
						Active?"":"// ", PnlHandleName, ConstNamePnlCtrl, 
						Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
						Active?"":"// ", Title); ShowC;
				break;
			
			
			///////////////////////////////////////////////////////////////////
			case CTRL_HSWITCH:
			case CTRL_VSWITCH:
			case CTRL_GROOVED_HSWITCH:
			case CTRL_GROOVED_VSWITCH:
			case CTRL_TOGGLE_HSWITCH:
			case CTRL_TOGGLE_VSWITCH:
				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_LABEL_TEXT, Title);	 
				strcpy(Title, Replace(Title, "__", ""));
				if (Title[0]=='\0') sprintf(Title, "Empty switch label (%s)", ConstNamePnlCtrl);
				sprintf(Str, "%d Switch %s \"%s\", %s %s %s", Ctrl, ConstNamePnlCtrl, Title, StrMode(Mode), Visible?"":"invisible", Dimmed?"dimmed":"");
				sprintf(StrHtml, "\n\n<!-- %s -->", Str); ShowH;
				sprintf(StrC, "\n\n\t// %s", Str);			ShowC;

				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_DATA_TYPE, &DataType);
				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_OFF_TEXT, OffText);
				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_ON_TEXT, OnText);
				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_DFLT_INDEX, &I);
				
				sprintf(StrHtml, "\n%s"
						"<%sINPUT TYPE=radio NAME=%s VALUE=\"0\"%s> %s "
						"<INPUT TYPE=radio NAME=%s VALUE=\"1\"%s> %s %s<BR>", 
						Title, Active?"":"!--", 
						ConstNamePnlCtrl, I==0?" CHECKED":"", OffText, 
						ConstNamePnlCtrl, I==1?" CHECKED":"", OnText,  Active?"":"--"); ShowH;
				sprintf(StrC, "\n\t%scgiFormRadio(\"%s\", Switch, 2, &I, %d);"
								"\n\t%sSetCtrlIndex(%s, %s, I);"
								"\n\t%s%s(%s, %s, EVENT_COMMIT, NULL, 0, 0);"
								"\n\t%sfprintf(cgiOut, \"\\n<P>%s set to %%d.</P>\", I);", 
						Active?"":"// ", ConstNamePnlCtrl, I,
						Active?"":"// ", PnlHandleName, ConstNamePnlCtrl, 
						Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
						Active?"":"// ", Title); ShowC;
				break;
			

			///////////////////////////////////////////////////////////////////
			case CTRL_RING:
			case CTRL_RECESSED_MENU_RING:
			case CTRL_MENU_RING:
			case CTRL_POPUP_MENU_RING:
			case CTRL_RING_VSLIDE:
			case CTRL_RING_HSLIDE:
			case CTRL_RING_FLAT_VSLIDE:
			case CTRL_RING_FLAT_HSLIDE:
			case CTRL_RING_LEVEL_VSLIDE:
			case CTRL_RING_LEVEL_HSLIDE:
			case CTRL_RING_POINTER_VSLIDE:
			case CTRL_RING_POINTER_HSLIDE:
			case CTRL_RING_THERMOMETER:
			case CTRL_RING_TANK:
			case CTRL_RING_GAUGE:
			case CTRL_RING_METER:
			case CTRL_RING_KNOB:
			case CTRL_RING_DIAL:
			case CTRL_PICTURE_RING:
				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_LABEL_TEXT, Title);	 
				strcpy(Title, Replace(Title, "__", ""));
				if (Title[0]=='\0') sprintf(Title, "Empty ring label (%s)", ConstNamePnlCtrl);
				sprintf(Str, "%d Ring %s \"%s\", %s %s %s", 
					Ctrl, ConstNamePnlCtrl, Title, StrMode(Mode), Visible?"":"invisible", Dimmed?"dimmed":"");
				sprintf(StrHtml, "\n\n<!-- %s -->", Str); ShowH;
				sprintf(StrC, "\n\n\t// %s", Str);		ShowC;

				GetNumListItems (PnlExplore, Ctrl, &NbItems);				
				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_DATA_TYPE, &DataType);
				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_DFLT_INDEX, &I);
				sprintf(StrHtml, "\n%s"
						"\n<%sSELECT NAME=%s>",
						Title, 
						Active?"":"!--", ConstNamePnlCtrl); ShowH;
				for (i=0; i<NbItems; i++) {
					GetLabelFromIndex (PnlExplore, Ctrl, i, Str);
					sprintf(StrHtml, "\n\t<OPTION VALUE=\"%d\"%s> %s", i, I==i?" SELECTED":"", Str); ShowH;
				}
				sprintf(StrHtml, "\n</SELECT><BR%s>", Active?"":"--"); ShowH;
				
				sprintf(StrC, "\n\t%scgiFormInteger(\"%s\", &I, %d);\t// use index, not value, easier but less secure"
								//cgiFormSelectSingle
								"\n\t%sSetCtrlIndex(%s, %s, I);"
								"\n\t%s%s(%s, %s, EVENT_COMMIT, NULL, 0, 0);"
								"\n\t%sfprintf(cgiOut, \"\\n<P>%s set to %%d.</P>\", I);", 
						Active?"":"// ", ConstNamePnlCtrl, I,
						Active?"":"// ", PnlHandleName, ConstNamePnlCtrl, 
						Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
						Active?"":"// ", Title); ShowC;
				break;
				
			
			
			///////////////////////////////////////////////////////////////////
			case CTRL_LIST:
				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_LABEL_TEXT, Title);	 
				strcpy(Title, Replace(Title, "__", ""));
				if (Title[0]=='\0') sprintf(Title, "Empty list label (%s)", ConstNamePnlCtrl);
				sprintf(Str, "%d List %s \"%s\", %s %s %s", 
					Ctrl, ConstNamePnlCtrl, Title, StrMode(Mode), Visible?"":"invisible", Dimmed?"dimmed":"");
				sprintf(StrHtml, "\n\n<!-- %s -->", Str); ShowH;
				sprintf(StrC, "\n\n\t// %s", Str);		ShowC;

				GetNumListItems (PnlExplore, Ctrl, &NbItems);				
				GetCtrlAttribute (PnlExplore, Ctrl, ATTR_DATA_TYPE, &DataType);
				sprintf(StrHtml, "\n%s"
						"\n<%sSELECT NAME=%s MULTIPLE>",
						Title, 
						Active?"":"!--", ConstNamePnlCtrl); ShowH;
				sprintf(StrC, "\n\t%schar* Possible%d[]={\"0\"", Active?"":"// ", Ctrl);
				for (i=0; i<NbItems; i++) {
					GetLabelFromIndex (PnlExplore, Ctrl, i, Str);
					IsListItemChecked (PnlExplore, Ctrl, i, &I);
					sprintf(StrHtml, "\n\t<OPTION VALUE=\"%d\"%s> %s", i, I?" SELECTED":"", Str); ShowH;
					if (i>0) sprintf(StrC, "%s,\"%d\"", StrC, i);
				}
				sprintf(StrHtml, "\n</SELECT><BR%s>", Active?"":"--"); ShowH;
				
				sprintf(StrC, "%s};\t// Move this line to the top of the function"
								"\n\t%sint Choices[%d];\t// Move this line to the top of the function"
								"\n\t%scgiFormCheckboxMultiple(\"%s\", Possible%d, %d, Choices, &Invalid);\t// use index, not value"
								"\n\t%sI=0;"
								"\n\t%sfor (i=0; i<%d; i++) { CheckListItem(%s, %s, i, Choices[i]); if (Choices[i]) I++; }"
								"\n\t%s%s(%s, %s, EVENT_COMMIT, NULL, 0, 0);"
								"\n\t%sfprintf(cgiOut, \"\\n<P>%s has %%d values selected.</P>\", I);", 
						StrC,
						Active?"":"// ", NbItems,
						Active?"":"// ", ConstNamePnlCtrl, Ctrl, NbItems, 
						Active?"":"// ", 
						Active?"":"// ", NbItems, PnlHandleName, ConstNamePnlCtrl,
						Active?"":"// ", CallbackName, PnlHandleName, ConstNamePnlCtrl,
						Active?"":"// ", Title); ShowC;
				break;
			
			///////////////////////////////////////////////////////////////////
			case CTRL_RAISED_BOX:
			case CTRL_RECESSED_BOX:
			case CTRL_FLAT_BOX:
			case CTRL_RAISED_CIRCLE:
			case CTRL_RECESSED_CIRCLE:
			case CTRL_FLAT_CIRCLE:
			case CTRL_RAISED_FRAME:
			case CTRL_RECESSED_FRAME:
			case CTRL_FLAT_FRAME:
			case CTRL_RAISED_ROUND_FRAME:
			case CTRL_RECESSED_ROUND_FRAME:
			case CTRL_FLAT_ROUND_FRAME:
			case CTRL_RAISED_ROUNDED_BOX:
			case CTRL_RECESSED_ROUNDED_BOX:
			case CTRL_FLAT_ROUNDED_BOX:
				break;
				
			///////////////////////////////////////////////////////////////////
			case CTRL_GRAPH:
				sprintf(StrHtml, "\n\n<!-- %d Graph %s not implemented -->", Ctrl, ConstNamePnlCtrl); ShowH;
				sprintf(StrC, "\n\n\t// %d Graph %s not implemented: %s", Ctrl, ConstNamePnlCtrl); ShowC;
				break;

			case CTRL_STRIP_CHART:
				sprintf(StrHtml, "\n\n<!-- %d Strip chart %s not implemented -->", Ctrl, ConstNamePnlCtrl); ShowH;
				sprintf(StrC, "\n\n\t// %d Strip chart %s not implemented: %s", Ctrl, ConstNamePnlCtrl); ShowC;
				break;

			case CTRL_PICTURE:
				sprintf(StrHtml, "\n\n<!-- %d Picture %s not implemented (save it in a jpg/gif/png file and use <IMG> -->", Ctrl, ConstNamePnlCtrl); ShowH;
				sprintf(StrC, "\n\n\t// %d Picture %s not implemented: %s", Ctrl, ConstNamePnlCtrl); ShowC;
				break;

			case CTRL_TIMER:
				sprintf(StrHtml, "\n\n<!-- %d Timer %s not implemented -->", Ctrl, ConstNamePnlCtrl); ShowH;
				sprintf(StrC, "\n\n\t// %d Timer %s not implemented: %s", Ctrl, ConstNamePnlCtrl); ShowC;
				break;

			case CTRL_CANVAS:
				sprintf(StrHtml, "\n\n<!-- %d Canvas %s not implemented -->", Ctrl, ConstNamePnlCtrl); ShowH;
				sprintf(StrC, "\n\n\t// %d Canvas %s not implemented: %s", Ctrl, ConstNamePnlCtrl); ShowC;
				break;

			case CTRL_TABLE:
				sprintf(StrHtml, "\n\n<!-- %d Table %s not implemented -->", Ctrl, ConstNamePnlCtrl); ShowH;
				sprintf(StrC, "\n\n\t// %d Table %s not implemented: %s", Ctrl, ConstNamePnlCtrl); ShowC;
				break;
		}
		GetCtrlAttribute(PnlExplore, Ctrl, ATTR_NEXT_CTRL, &Ctrl);
	}
	sprintf(StrHtml, "\n</FIELDSET>\n"); ShowH;
	SetCtrlVal(Pnl, PANEL_C_CODE, StrButton); WriteFile(CFile, StrButton, strlen(StrButton));
	sprintf(StrC, "\n}"); ShowC;
}

int CVICALLBACK CB_Select (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int PanelConst=0, PnlExplore=-1, i;
	char UIRfilePath[MAX_PATHNAME_LEN], HtmlFilePath[MAX_PATHNAME_LEN], CFilePath[MAX_PATHNAME_LEN], 
			FileName[MAX_FILENAME_LEN], *Pos;
	switch (event) {
		case EVENT_COMMIT:
			if (FileSelectPopup ("", "*.uir", "*.uir", "Select UIR file to analyse",
							 VAL_LOAD_BUTTON, 0, 1, 1, 0, UIRfilePath)!=VAL_EXISTING_FILE_SELECTED)
				return 0;
			strcpy(HtmlFilePath, UIRfilePath);
			strcpy(CFilePath, UIRfilePath);
			Pos=strrchr(HtmlFilePath, '.');	strcpy(Pos, "2html.html");
			Pos=strrchr(CFilePath, '.');	strcpy(Pos, "2c.c");
			HtmlFile = OpenFile (HtmlFilePath, VAL_WRITE_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
			CFile = OpenFile (CFilePath, VAL_WRITE_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
			sprintf(StrHtml, "HTML code in %s",  HtmlFilePath);
			SetCtrlAttribute (Pnl, PANEL_HTML_CODE, ATTR_LABEL_TEXT, StrHtml);
			sprintf(StrC, "C code in %s",  CFilePath);
			SetCtrlAttribute (Pnl, PANEL_C_CODE, ATTR_LABEL_TEXT, StrC);
			
			SplitPath (UIRfilePath, NULL, NULL, FileName);
			sprintf(StrHtml, "<HTML>"
							"\n<HEAD>"
							"\n<TITLE>Form generated from %s</TITLE>"
							"\n</HEAD>"
							"\n<BODY>"
							"\n<H1>Form generated from %s</H1>"
							"\n<H2>By PanelToForm.exe, (c) 2000 Guillaume Dargaud</H2>\n", 
					FileName, FileName); ShowH;
			Pos = strrchr (FileName, '.'); *Pos='\0';
			sprintf(StrHtml, "\n<FORM METHOD=\"POST\" ACTION=\"%s.exe\"> <!-- or Capture.exe -->\n", FileName); ShowH;
			sprintf(StrC, "// C code generated by PanelToForm.exe from %s.uir, (c) 2000 Guillaume Dargaud"
						"\n#include <ansi_c.h>"
						"\n#include \"cgic.h\""
						"\n#include \"%s.h\""
						"// Link this file with CGIC.C, %s.UIR and the C file containing the callback functions",
						FileName, FileName, FileName); ShowC;
			*Pos='.';
		
			DisableBreakOnLibraryErrors ();
			SetSystemAttribute(ATTR_ALLOW_MISSING_CALLBACKS, 1);
			while ((PnlExplore = LoadPanel (0, UIRfilePath, ++PanelConst))>=0) {
				EnableBreakOnLibraryErrors ();
				DisplayPanel(PnlExplore);
				ExplorePanel(PnlExplore, PanelConst, FileName);
				DiscardPanel(PnlExplore);
				PnlExplore=-1;
				DisableBreakOnLibraryErrors ();
			}
			EnableBreakOnLibraryErrors ();
			sprintf(StrHtml, "\n\n<INPUT TYPE=submit VALUE=\"SEND\">\n"
							"<INPUT TYPE=reset VALUE=\"CLEAR\">\n"
							"</FORM>\n</BODY>\n</HTML>\n"); ShowH;
			sprintf(StrC, "\n\nint cgiMain (void) {"
							"\n#ifdef _CVI_DEBUG_"
							"\n\tcgiReadEnvironment(\"capcgi.dat\");"
							"\n#endif"
							"\n\tcgiHeaderContentType(\"text/html\");"
							"\n\tfprintf(cgiOut, \"<HTML>\\n<HEAD>\""
							"\n\t\t\"\\n<TITLE>PanelToForm and cgic test</TITLE>\\n</HEAD>\""
							"\n\t\t\"\\n<BODY>\\n<H1>PanelToForm and cgic test</H1>\""
							"\n\t\t\"\\n<H2>Generated by PanelToForm.exe, (c) 2000 Guillaume Dargaud</H2>\");"
			); ShowC;
			for (i=1; i<PanelConst; i++) { sprintf(StrC, "\n\tInterpretPanel%d();", i);	 ShowC; } 
			sprintf(StrC, "\n\tfprintf(cgiOut, \"\\n</BODY>\\n</HTML>\\n\");"
							"\n\treturn 0;\n}\n");	 ShowC;

			SetCommitMode(1);
			CloseFile(HtmlFile);
			CloseFile(CFile);
			break;
		case EVENT_RIGHT_CLICK:

			break;
	}
	return 0;
}

int CVICALLBACK CB_Quit (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			QuitUserInterface (0);
			break;
		case EVENT_RIGHT_CLICK:

			break;
	}
	return 0;
}

int CVICALLBACK CBP_Panel (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	int Width, Height, CtrlHeight, Top;
	switch (event) {
		case EVENT_PANEL_SIZE:
			GetPanelAttribute(Pnl, ATTR_WIDTH, &Width);
			GetPanelAttribute(Pnl, ATTR_HEIGHT, &Height);
			
			SetCtrlAttribute(Pnl, PANEL_HTML_CODE, ATTR_WIDTH, Width);
			SetCtrlAttribute(Pnl, PANEL_C_CODE, ATTR_WIDTH, Width);
			
			GetCtrlAttribute(Pnl, PANEL_HTML_CODE, ATTR_TOP, &Top);
			CtrlHeight=(Height-20-Top)/2;
			SetCtrlAttribute(Pnl, PANEL_HTML_CODE, ATTR_HEIGHT, CtrlHeight);
			SetCtrlAttribute(Pnl, PANEL_C_CODE, ATTR_HEIGHT, CtrlHeight);
			SetCtrlAttribute(Pnl, PANEL_C_CODE, ATTR_TOP, Top+CtrlHeight+20);
			break;
	}
	return 0;
}

