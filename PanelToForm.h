/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2000. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PANEL                           1       /* callback function: CBP_Panel */
#define  PANEL_QUIT                      2       /* callback function: CB_Quit */
#define  PANEL_SELECT                    3       /* callback function: CB_Select */
#define  PANEL_C_CODE                    4
#define  PANEL_HTML_CODE                 5
#define  PANEL_TEXTMSG                   6


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK CBP_Panel(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Quit(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Select(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
