PROGRAM:   PanelToForm.exe                                                        

VERSION:   1.0                                                                 

AUTHOR:    Guillaume Dargaud - Freeware                                 

PURPOSE:   Takes a CVI 5.5 User Interface Resource file and creates:  
            - an HTML file containing forms corresponding to the inputs of the UIR  
            - a C source file containing the corres�ponding calls to set the values of the UIR as set by the HTML form when calling the exe file as a cgi-script.  
            In other words, this freeware helps you put your CVI applications on the web !!!

INSTALL:   run SETUP. 
            You NEED to install the LabWindows/CVI runtime engine on your PC.
            It is available frmo http://ni.com/

NEEDED:    To link the c code, you need the CGIC v1.06 library available at http://www.boutell.com/cgic/

HELP:      available by right-clicking an item on the user interface.

TUTORIAL:  http://www.gdargaud.net/Hack/PanelToCgi.html

HISTORY:   v1.0 - 17/1/2000 - Original release

KNOWN BUG: This has not been extensively tested, so it's probably full of bugs.  
            Anyway, this program is a tool to *help* you transform your CVI applications to a cgi script.

LICENSE:   GPLv3
