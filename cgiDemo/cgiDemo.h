/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2000. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PNLTEST                         1
#define  PNLTEST_DIMENSIONS              2       /* callback function: CB_Dimensions */
#define  PNLTEST_TEMPERATURE             3       /* callback function: CB_Temperature */
#define  PNLTEST_CONNECTIONS             4       /* callback function: CB_Connections */
#define  PNLTEST_NAME                    5       /* callback function: CB_Name */
#define  PNLTEST_ADDRESS                 6       /* callback function: CB_Address */
#define  PNLTEST_ACTIVE                  7       /* callback function: CB_Active */
#define  PNLTEST_LED                     8       /* callback function: CB_Led */
#define  PNLTEST_STATUS                  9       /* callback function: CB_Status */
#define  PNLTEST_STATE                   10      /* callback function: CB_State */
#define  PNLTEST_VERIFY                  11      /* callback function: CB_Verify */
#define  PNLTEST_MENU                    12      /* callback function: CB_Menu */
#define  PNLTEST_SHOPPING                13      /* callback function: CB_Shopping */
#define  PNLTEST_TEXTMSG                 14


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK CB_Active(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Address(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Connections(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Dimensions(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Led(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Menu(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Name(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Shopping(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_State(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Status(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Temperature(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Verify(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
